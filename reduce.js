function reduce(elements, cb, startingValue) {
  if (Array.isArray(elements)) {
    if (startingValue === undefined) {
      startingValue = elements[0];
    }
    for (let index = 0; index < elements.length; index++) {
      startingValue = cb(startingValue, elements[index]);
    }
    return startingValue;
  } else {
    return null;
  }
}

function cb(startingValue, item) {
  startingValue += item;

  return startingValue;
}

module.exports = { reduce, cb };
