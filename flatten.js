const nestedArray = [1, [2], [[3]], [[[4]]]]; // use this to test 'flatten'

const newArray = [];

function flatten(elements) {
  for (let index = 0; index < elements.length; index++) {
    if (Array.isArray(elements[index])) {
      flatten(elements[index]);
    } else {
      newArray.push(elements[index]);
    }
  }
  return newArray;
}

module.exports = flatten;
