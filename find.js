function find(elements, cb) {
  if (Array.isArray(elements)) {
    for (let index = 0; index < elements.length; index++) {
      let found = cb(elements[index]);
      if (found)
      {
        return elements[index];
      }
    }
    return undefined;
    
  } else {
    return null;
  }
}
function cb(item) {
  if (item > 3) {
    return true; //To find item greater than 3
  }
}

module.exports = { find, cb };
