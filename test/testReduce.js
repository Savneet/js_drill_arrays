const { reduce, cb } = require("../reduce.js");
const items = [1, 2, 3, 4, 5, 5]; // use this array to test your code.

try {
  console.log(reduce(items, cb, 0));//Starting value is 0  
} catch (error) {
  console.log("There is some error in code");
}
