const flatten = require("../flatten.js");
const nestedArray = [1, [2], [[3]], [[[4]]]]; // use this to test 'flatten'

try {
  console.log(flatten(nestedArray));  
} catch (error) {
  console.log("There is some error in code");
}
