function each(elements, cb) {
    
    if(Array.isArray(elements))
    {
        for(let index=0; index<elements.length;index++)
        {
            cb(elements[index],index);
        }
    }
    else
    {
        return null;
    }
}
function cb(item,itemIndex)
{
    console.log(`Item: ${item}, Index: ${itemIndex}`);
}

module.exports={each, cb};

